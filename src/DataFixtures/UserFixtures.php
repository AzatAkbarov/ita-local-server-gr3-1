<?php

namespace App\DataFixtures;


use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    public function load(ObjectManager $manager)
    {

        $user1 = $this->userHandler->createNewUser([
            'email' => 'asd@asd.asd',
            'passport' => 'AN12345678',
            'password' => '213471118',
        ]);

        $manager->persist($user1);

        $user2 = $this->userHandler->createNewUser([
            'email' => 'qwe@qwe.qwe',
            'passport' => 'AN87654321',
            'password' => '1123581321',
        ]);

        $manager->persist($user2);

        $user3 = $this->userHandler->createNewUser([
            'email' => 'zxc@zxc.zxc',
            'passport' => 'AN12344321',
            'password' => '32571219',
        ]);

        $manager->persist($user3);
        $manager->flush();
    }
}
